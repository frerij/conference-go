import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY

import json

def find_pictures(search_term):
    url = "https://api.pexels.com/v1/search?query=" + search_term
    headers = {"Authorization": PEXELS_API_KEY}
    response = requests.get(url, headers=headers)
    data = json.loads(response.content)
    photos = data["photos"]

    #returns first photo
    return photos[0]

def get_weather_data(city, state):
    
    url = "http://api.openweathermap.org/geo/1.0/direct?q=" + city + "," + state + ",US&limit=5&appid=" + OPEN_WEATHER_API_KEY
    response = requests.get(url)
    data = json.loads(response.content)[0]
    lon = data["lon"]
    lat = ["lat"]

    # now use lon and lat to get weather data and then return weather data
    weather_url = f'https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}'
    return{}